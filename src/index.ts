import { console, AppConsole, BackOrExit } from '@sgtools/console';

export enum ActionType
{
    Default  = 'default',
    Function = 'function'
}

export interface AppConsoleActionItem<T>
{
    type     : ActionType;
    name     : string;
    aliases ?: string[];
    check   ?: (action: string) => boolean;
    cb       : (action?: string) => Promise<T>;
}

export class AppConsoleAction<T>
{
    actions: AppConsoleActionItem<T>[];

    constructor() {
        this.actions = [];
    }

    public init()
    {
        this.actions = [];
    }

    public addAction(name: string, aliases: string[], cb: (action?: string) => Promise<T>)
    {
        if (aliases.length === 0) return;
        this.actions.push({
            type    : ActionType.Default,
            name    : (name == '' ? aliases[0] : name),
            aliases : aliases,
            cb      : cb
        });
    }

    public addActionFn(name: string, check: (action: string) => boolean, cb: (action?: string) => Promise<T>)
    {
        this.actions.push({
            type    : ActionType.Function,
            name    : name,
            check   : check,
            cb      : cb
        });
    }

    public addBack(condition?: boolean, data?: any,)
    {
        if (typeof condition !== 'undefined' && !condition) return;

        this.addAction(
            'b(ack)',
            ['b', 'back'],
            async () => {
                return { back: true, exit: false, ...data };
            }
        );
    }

    public addQuit(condition?: boolean, data?: any)
    {
        if (typeof condition !== 'undefined' && !condition) return;
        
        this.addAction(
            'q(uit)',
            ['q', 'quit'],
            async () => {
                return { back: false, exit: true, ...data };
            }
        );
    }

    public loop(cb: () => Promise<BackOrExit>): Promise<BackOrExit>
    {
        return this.show().then((res: T) => {
            if (res['back'] || res['exit']) {
                //return { back: res['back'], exit: res['exit'] };
                return { back: false, exit: res['exit'] };
            } else {
                return cb();
            }
        });
    }

    public show(): Promise<T>
    {
        return this.showOnce().then(res => {
            if (res.status) {
                return res.data;
            } else {
                return this.show();
            }
        });
    }

    private showOnce(): Promise<{ status: boolean, data: T }>
    {
        return new Promise((resolve, reject) => {
            console.question(this.getQuestion()).then((result: string) => {
                let action = result.trim();
                let hasRun = false;
                this.actions.forEach(a => {
                    if (this.check(a, action)) {
                        hasRun = true;
                        a.cb(action).then(res => {
                            resolve({ status: true, data: res });
                        });
                    }
                });
                if (!hasRun) {
                    console.error('Incorrect command');
                    resolve({ status: false, data: null });
                }
            });
        });
    }

    private check(item: AppConsoleActionItem<T>, action: string)
    {
        if (item.type === ActionType.Default) {
            return item.aliases.includes(action);
        }
        if (item.type === ActionType.Function) {
            return item.check(action);
        }
        return false;
    }

    private getQuestion()
    {
        return 'Action [ ' + this.actions.map(a => a.name).join(' | ') + ' ]';
    }
}

declare module '@sgtools/console'
{
    export interface BackOrExit
    {
        back: boolean
        exit: boolean
    }

    export interface AppConsole
    {
        createActions<T>(): AppConsoleAction<T>
        waitForQuit(): Promise<BackOrExit>;
    }
}
AppConsole.prototype.createActions = <T>() =>
{
    return new AppConsoleAction<T>();
}
AppConsole.prototype.waitForQuit = () =>
{
    let actions = console.createActions();
    actions.addQuit();
    return actions.show().then(() => { return { back: false, exit: true }; });
}
