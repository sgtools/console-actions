import { BackOrExit } from '@sgtools/console';
export declare enum ActionType {
    Default = "default",
    Function = "function"
}
export interface AppConsoleActionItem<T> {
    type: ActionType;
    name: string;
    aliases?: string[];
    check?: (action: string) => boolean;
    cb: (action?: string) => Promise<T>;
}
export declare class AppConsoleAction<T> {
    actions: AppConsoleActionItem<T>[];
    constructor();
    init(): void;
    addAction(name: string, aliases: string[], cb: (action?: string) => Promise<T>): void;
    addActionFn(name: string, check: (action: string) => boolean, cb: (action?: string) => Promise<T>): void;
    addBack(condition?: boolean, data?: any): void;
    addQuit(condition?: boolean, data?: any): void;
    loop(cb: () => Promise<BackOrExit>): Promise<BackOrExit>;
    show(): Promise<T>;
    private showOnce;
    private check;
    private getQuestion;
}
declare module '@sgtools/console' {
    interface BackOrExit {
        back: boolean;
        exit: boolean;
    }
    interface AppConsole {
        createActions<T>(): AppConsoleAction<T>;
        waitForQuit(): Promise<BackOrExit>;
    }
}
