"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppConsoleAction = exports.ActionType = void 0;
const console_1 = require("@sgtools/console");
var ActionType;
(function (ActionType) {
    ActionType["Default"] = "default";
    ActionType["Function"] = "function";
})(ActionType = exports.ActionType || (exports.ActionType = {}));
class AppConsoleAction {
    constructor() {
        this.actions = [];
    }
    init() {
        this.actions = [];
    }
    addAction(name, aliases, cb) {
        if (aliases.length === 0)
            return;
        this.actions.push({
            type: ActionType.Default,
            name: (name == '' ? aliases[0] : name),
            aliases: aliases,
            cb: cb
        });
    }
    addActionFn(name, check, cb) {
        this.actions.push({
            type: ActionType.Function,
            name: name,
            check: check,
            cb: cb
        });
    }
    addBack(condition, data) {
        if (typeof condition !== 'undefined' && !condition)
            return;
        this.addAction('b(ack)', ['b', 'back'], async () => {
            return Object.assign({ back: true, exit: false }, data);
        });
    }
    addQuit(condition, data) {
        if (typeof condition !== 'undefined' && !condition)
            return;
        this.addAction('q(uit)', ['q', 'quit'], async () => {
            return Object.assign({ back: false, exit: true }, data);
        });
    }
    loop(cb) {
        return this.show().then((res) => {
            if (res['back'] || res['exit']) {
                //return { back: res['back'], exit: res['exit'] };
                return { back: false, exit: res['exit'] };
            }
            else {
                return cb();
            }
        });
    }
    show() {
        return this.showOnce().then(res => {
            if (res.status) {
                return res.data;
            }
            else {
                return this.show();
            }
        });
    }
    showOnce() {
        return new Promise((resolve, reject) => {
            console_1.console.question(this.getQuestion()).then((result) => {
                let action = result.trim();
                let hasRun = false;
                this.actions.forEach(a => {
                    if (this.check(a, action)) {
                        hasRun = true;
                        a.cb(action).then(res => {
                            resolve({ status: true, data: res });
                        });
                    }
                });
                if (!hasRun) {
                    console_1.console.error('Incorrect command');
                    resolve({ status: false, data: null });
                }
            });
        });
    }
    check(item, action) {
        if (item.type === ActionType.Default) {
            return item.aliases.includes(action);
        }
        if (item.type === ActionType.Function) {
            return item.check(action);
        }
        return false;
    }
    getQuestion() {
        return 'Action [ ' + this.actions.map(a => a.name).join(' | ') + ' ]';
    }
}
exports.AppConsoleAction = AppConsoleAction;
console_1.AppConsole.prototype.createActions = () => {
    return new AppConsoleAction();
};
console_1.AppConsole.prototype.waitForQuit = () => {
    let actions = console_1.console.createActions();
    actions.addQuit();
    return actions.show().then(() => { return { back: false, exit: true }; });
};
