[@sgtools/console-actions](../README.md) / [Exports](../modules.md) / ActionType

# Enumeration: ActionType

## Table of contents

### Enumeration members

- [Default](actiontype.md#default)
- [Function](actiontype.md#function)

## Enumeration members

### Default

• **Default**: = "default"

Defined in: index.ts:5

___

### Function

• **Function**: = "function"

Defined in: index.ts:6
