[@sgtools/console-actions](README.md) / Exports

# @sgtools/console-actions

## Table of contents

### Enumerations

- [ActionType](enums/actiontype.md)

### Classes

- [AppConsoleAction](classes/appconsoleaction.md)

### Interfaces

- [AppConsoleActionItem](interfaces/appconsoleactionitem.md)
