[@sgtools/console-actions](../README.md) / [Exports](../modules.md) / AppConsoleActionItem

# Interface: AppConsoleActionItem<T\>

## Type parameters

Name |
------ |
`T` |

## Hierarchy

* **AppConsoleActionItem**

## Table of contents

### Properties

- [aliases](appconsoleactionitem.md#aliases)
- [cb](appconsoleactionitem.md#cb)
- [check](appconsoleactionitem.md#check)
- [name](appconsoleactionitem.md#name)
- [type](appconsoleactionitem.md#type)

## Properties

### aliases

• `Optional` **aliases**: *string*[]

Defined in: index.ts:13

___

### cb

• **cb**: (`action?`: *string*) => *Promise*<T\>

Defined in: index.ts:15

___

### check

• `Optional` **check**: (`action`: *string*) => *boolean*

Defined in: index.ts:14

___

### name

• **name**: *string*

Defined in: index.ts:12

___

### type

• **type**: [*ActionType*](../enums/actiontype.md)

Defined in: index.ts:11
