[@sgtools/console-actions](../README.md) / [Exports](../modules.md) / AppConsoleAction

# Class: AppConsoleAction<T\>

## Type parameters

Name |
------ |
`T` |

## Hierarchy

* **AppConsoleAction**

## Table of contents

### Constructors

- [constructor](appconsoleaction.md#constructor)

### Properties

- [actions](appconsoleaction.md#actions)

### Methods

- [addAction](appconsoleaction.md#addaction)
- [addActionFn](appconsoleaction.md#addactionfn)
- [addBack](appconsoleaction.md#addback)
- [addQuit](appconsoleaction.md#addquit)
- [check](appconsoleaction.md#check)
- [getQuestion](appconsoleaction.md#getquestion)
- [init](appconsoleaction.md#init)
- [loop](appconsoleaction.md#loop)
- [show](appconsoleaction.md#show)
- [showOnce](appconsoleaction.md#showonce)

## Constructors

### constructor

\+ **new AppConsoleAction**<T\>(): [*AppConsoleAction*](appconsoleaction.md)<T\>

#### Type parameters:

Name |
------ |
`T` |

**Returns:** [*AppConsoleAction*](appconsoleaction.md)<T\>

Defined in: index.ts:20

## Properties

### actions

• **actions**: [*AppConsoleActionItem*](../interfaces/appconsoleactionitem.md)<T\>[]

Defined in: index.ts:20

## Methods

### addAction

▸ **addAction**(`name`: *string*, `aliases`: *string*[], `cb`: (`action?`: *string*) => *Promise*<T\>): *void*

#### Parameters:

Name | Type |
------ | ------ |
`name` | *string* |
`aliases` | *string*[] |
`cb` | (`action?`: *string*) => *Promise*<T\> |

**Returns:** *void*

Defined in: index.ts:31

___

### addActionFn

▸ **addActionFn**(`name`: *string*, `check`: (`action`: *string*) => *boolean*, `cb`: (`action?`: *string*) => *Promise*<T\>): *void*

#### Parameters:

Name | Type |
------ | ------ |
`name` | *string* |
`check` | (`action`: *string*) => *boolean* |
`cb` | (`action?`: *string*) => *Promise*<T\> |

**Returns:** *void*

Defined in: index.ts:42

___

### addBack

▸ **addBack**(`condition?`: *boolean*, `data?`: *any*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`condition?` | *boolean* |
`data?` | *any* |

**Returns:** *void*

Defined in: index.ts:52

___

### addQuit

▸ **addQuit**(`condition?`: *boolean*, `data?`: *any*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`condition?` | *boolean* |
`data?` | *any* |

**Returns:** *void*

Defined in: index.ts:65

___

### check

▸ `Private`**check**(`item`: [*AppConsoleActionItem*](../interfaces/appconsoleactionitem.md)<T\>, `action`: *string*): *boolean*

#### Parameters:

Name | Type |
------ | ------ |
`item` | [*AppConsoleActionItem*](../interfaces/appconsoleactionitem.md)<T\> |
`action` | *string* |

**Returns:** *boolean*

Defined in: index.ts:123

___

### getQuestion

▸ `Private`**getQuestion**(): *string*

**Returns:** *string*

Defined in: index.ts:134

___

### init

▸ **init**(): *void*

**Returns:** *void*

Defined in: index.ts:26

___

### loop

▸ **loop**(`cb`: () => *Promise*<BackOrExit\>): *Promise*<BackOrExit\>

#### Parameters:

Name | Type |
------ | ------ |
`cb` | () => *Promise*<BackOrExit\> |

**Returns:** *Promise*<BackOrExit\>

Defined in: index.ts:78

___

### show

▸ **show**(): *Promise*<T\>

**Returns:** *Promise*<T\>

Defined in: index.ts:90

___

### showOnce

▸ `Private`**showOnce**(): *Promise*<{ `data`: T ; `status`: *boolean*  }\>

**Returns:** *Promise*<{ `data`: T ; `status`: *boolean*  }\>

Defined in: index.ts:101
